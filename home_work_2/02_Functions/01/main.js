let getLastDigitAsWord = (number) => {
    let digitNames = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
    return digitNames[number % 10];
};
let number = 1234;
console.log(`last digit of ${number} is ${getLastDigitAsWord(number)}`);