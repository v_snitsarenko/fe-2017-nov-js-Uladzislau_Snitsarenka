let isBiggerThenNeighbours = (array, position) => {
    if (!(position in array)) {
        return false;
    } else {
        return ((array[position] > array[position + 1]) && (array[position] > array[position - 1]));
    }
};
let indexOfBiggerThenNeighbours = (array) => {
    for (let i = 0; i < array.length; i++) {
        if (isBiggerThenNeighbours(array, i)) return i;
    }
    return -1;
};
let array = [1, 1, 3, 8, 3, 3, 2, 3, 3];
console.log(`In [${array}] index of first bigger then neighbours element = ${indexOfBiggerThenNeighbours(array)}`);