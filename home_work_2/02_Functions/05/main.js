let countRepeatingNumber = (array, number) => array.filter((elem) => elem === number).length;
let repeatingNumberTest = (testsCount) => {
    let ARRAYS_LENGTH = 20;
    let testArrays = new Array(testsCount);
    for (let i = 0; i < testArrays.length; i++) {
        let tempArr = new Array(ARRAYS_LENGTH);
        for (let j = 0; j < ARRAYS_LENGTH; j++) {
            tempArr[j] = Math.floor(Math.random() * 10);
        }
        testArrays[i] = tempArr;
    }
    for (let i = 0; i < testArrays.length; i++) {
        let number = Math.floor(Math.random() * 10);
        console.log(`value ${number} in array [${testArrays[i]}] appears ${countRepeatingNumber(testArrays[i], number)} times`);
    }
};
repeatingNumberTest(15);