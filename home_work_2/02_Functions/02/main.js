let reverseDigits = (number) => parseInt(number.toString().split('').reverse().join(''));
console.log(`1234 reversed -> ${reverseDigits(1234)}`);