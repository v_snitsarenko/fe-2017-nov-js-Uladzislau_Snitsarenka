let countRepeatingWord = (text, word, type) => {
    let reg = type === 'i' ? new RegExp('\\b' + word + '\\b', 'gi') : new RegExp('\\b' + word + '\\b', 'g');
    let result = text.match(reg);
    return result ? result.length : -1;
};
let text = 'Hello world. World is big. World are great';
console.log(text);
let word = 'world';
console.log(`Insensitive search: In text ${word} occurs ${countRepeatingWord(text, word, 'i')} times`);
console.log(`Case-sensitive search: In text ${word} occurs ${countRepeatingWord(text, word)} times`);