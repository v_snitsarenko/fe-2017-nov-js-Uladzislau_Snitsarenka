let isBiggerThenNeighbours = (array, position) => {
    if (!(position in array)) {
        return false;
    } else {
        return ((array[position] > array[position + 1]) && (array[position] > array[position - 1]));
    }
};
let array = [5, 2, 3, 4, 3, 3, 2, 3, 3];
let index = 3;
console.log(`Element with index ${index} in [${array}] ${isBiggerThenNeighbours(array, index) ? 'is bigger then neighbours' : 'is not bigger then neighbours'}`);
