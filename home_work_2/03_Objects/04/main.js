function hasProperty(obj, property) {
    return property in obj;
}

let object = {'prop': 12, prop2: 1234};
let property = 'prop';
console.log('Object ', object, `${hasProperty(object, property) ?
    'has property ' + property : 'don\'t have property ' + property} `);