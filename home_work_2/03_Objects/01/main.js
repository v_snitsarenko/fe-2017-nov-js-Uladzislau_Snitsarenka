class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    static distance(a, b) {
        const dx = b.x - a.x;
        const dy = b.y - a.y;

        return Math.hypot(dx, dy);
    }

    toString() {
        return `{x:${this.x}, y:${this.y}}`
    }
}

class Line {
    constructor(start, end) {
        this.start = start;
        this.end = end;
    }

    getLength() {
        return Point.distance(this.start, this.end);
    }

    toString() {
        return `{start:${this.start}, end:${this.end}}`
    }
}

let isTriangle = (a, b, c) => {
    return ((a.getLength() < b.getLength() + c.getLength() &&
            b.getLength() < c.getLength() + a.getLength() &&
            c.getLength() < a.getLength() + b.getLength())
    )
};
let p1 = new Point(2, 3);
let p2 = new Point(5, 6);
console.log(`Distance between point ${p1} and point ${p2} = ${Point.distance(p1, p2)}`);
let l1 = new Line(new Point(0, 0), new Point(2, 2),
    l2 = new Line(new Point(0, 0), new Point(2, 2)),
    l3 = new Line(new Point(0, 0), new Point(2, 2)));
console.log(`From lines ${l1} and ${l2} and ${l3} ${isTriangle(l1, l2, l3) ? 'you can create triangle' : 'you can\'t create triangle'}`);
