let getYoungestPersonFullName = (persons) => {
    let youngestPerson = persons.sort((a, b) => a.age - b.age)[0];
    return youngestPerson.firstName + ' ' + youngestPerson.lastName;
};
let persons = [{firstName: "Gosho", lastName: "Petrov", age: 32},
    {firstName: "Ivan", lastName: "Sidorov", age: 42}, {firstName: "Vasya", lastName: "Assassin", age: 22}];
console.log(`Youngest person from`, persons, `is ${getYoungestPersonFullName(persons)}`);
