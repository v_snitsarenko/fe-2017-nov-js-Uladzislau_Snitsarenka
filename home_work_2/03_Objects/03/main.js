function deepCopy(obj) {
    return JSON.parse(JSON.stringify(obj));
}

let original = {prop: 123, prop2: [1, 2, 3, 4, 5], prop3: {'prop3': 1234}};

console.log('Original object\n', original);
let copy = deepCopy(original);

console.log('Copy after copy\n', copy);
original.prop4 = 'qwer';

console.log('Original changed\n', original);
console.log('Copy after original changes\n', copy);
