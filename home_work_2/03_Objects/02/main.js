Array.prototype.remove = function (value) {
    return this.filter((elem) => elem !== value);
};
let array = [1, 2, 3, 4, 5, 1, 1, 1, 2, 3];
console.log(`before remove [${array}]`);
let value = 1;
console.log(`removed ${value} = > [${array.remove(value)}]`);