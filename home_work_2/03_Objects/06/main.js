let group = (persons, option) => {
    let groupedObj = {};
    switch (option) {
        case 'firstname':
            persons.forEach((item) => item.firstName in groupedObj ? groupedObj[item.firstName].push(item) : groupedObj[item.firstName] = [item]);
            break;
        case 'lastname':
            persons.forEach((item) => item.lastName in groupedObj ? groupedObj[item.lastName].push(item) : groupedObj[item.lastName] = [item]);
            break;
        case 'age':
            persons.forEach((item) => item.age in groupedObj ? groupedObj[item.age].push(item) : groupedObj[item.age] = [item]);
            break;
    }
    return groupedObj;
};
let persons = [{firstName: "Gosho", lastName: "Petrov", age: 32}, {firstName: "Ivan", lastName: "Petrov", age: 12},
    {firstName: "Fedor", lastName: "Sidorov", age: 12}];
console.log('Before group', persons);
console.log(group(persons, 'firstname'));
console.log(group(persons, 'lastname'));
console.log(group(persons, 'age'));