function replaceTagA(html) {
    return html.replace(/<a href="(.*?)">(.*?)<\/a>/g, '[URL=$1]$2[/URL]');
}
let text = '<p>Please visit <a href="http://academy.telerik.com">our site</a> to choose a training course. Also visit <a href="www.devbg.org">our forum</a> to discuss the courses.</p>';
console.log(text);
console.log(replaceTagA(text));
