function getPalindromes(text) {
    let arr = text.match(/\b\w{3,}\b/g);
    return arr.filter((elem) => (elem === elem.split('').reverse().join('')));
}
let text = 'text abba lamal exe wqeqwe dad qq';
console.log(text);
console.log('[ '+getPalindromes(text)+' ]');