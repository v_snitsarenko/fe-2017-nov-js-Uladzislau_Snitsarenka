let substringCount = (text, substring) => text.toLowerCase().split(substring).length - 1;

let text = 'We are living in an yellow submarine. We don\'t have anything else. ' +
    'Inside the submarine is very tight. So we are drinking all the day. We will move out of it in 5 days.';
let substr = 'in';
console.log(text);
console.log(`In this text substring '${substr}' appears ${substringCount(text, substr) } times`);
