let generateList = (peoples, template) => {
    let html = '<ul>';
    for (let key in peoples) {
        if (peoples.hasOwnProperty(key)) {
            html += '<li>';
            html += template.replace(/-\{(\w+)\}-/g, (match, res) => peoples[key][res]);
            html += '</li>';
        }
    }
    html += '</ul>';
    return html;
};
let peoples = [{name: "Peter", age: 14}, {name: "Ivan", age: 12}, {name: "Vasya", age: 22}, {name: "Egor", age: 32}];
let tmpl = document.getElementById("list-item").innerHTML;
let peopleList = generateList(peoples, tmpl);
console.log(peopleList);
document.getElementById('peoplesList').innerHTML = peopleList;