function format(string, ...args) {
    return string.replace(/{(\d+)}/g, (match, res) => args[res]);
}

let string = 'Hello {1} {0}!', param0 = 'Peter', param1 = 'dear';
console.log(string, param0, param1);
console.log(format(string, param0, param1));