function checkBrackets(exp) {
    let count = 0;
    for (let i = 0; i < exp.length && count >= 0; i++) {
        if (exp.charAt(i) === '(') count++;
        if (exp.charAt(i) === ')') count--;
    }
    return !count;
}

let expression = '((a+b)/5-d)';
console.log('Expression', expression, 'is', checkBrackets(expression) ? 'good' : 'bad');