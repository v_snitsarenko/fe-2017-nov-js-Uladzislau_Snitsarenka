function replaceSpaces(text) {
    return text.replace(new RegExp('\u00A0', 'g'), (match, res) => '&nbsp;');
}

let text = 'text  text!   big    bad  wolf ';
console.log(text);
console.log(replaceSpaces(text));