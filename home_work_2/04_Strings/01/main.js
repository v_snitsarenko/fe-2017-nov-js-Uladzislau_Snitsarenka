let reverseString = (string) => string.split('').reverse().join('');
let string = 'sample';
let string2 = 'tpircSavaJ';
console.log(`original -> ${string}, reversed -> ${reverseString(string)}`);
console.log(`original -> ${string2}, reversed -> ${reverseString(string2)}`);