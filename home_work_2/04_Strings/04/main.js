function changeText(text) {
    let regUpper = new RegExp('<upcase>(.*?)</upcase>', 'gi');
    let regLower = new RegExp('<lowcase>(.*?)</lowcase>', 'gi');
    let regMix = new RegExp('<mixcase>(.*?)</mixcase>', 'gi');
    let toMixCase = (text) => {
        let str = '';
        for (let i = 0; i < text.length; i++) {
            if (Math.random() > 0.5) {
                str += text.charAt(i).toLowerCase();
            } else
                str += text.charAt(i).toUpperCase();
        }
        return str;
    };
    return text.replace(regUpper, (match, res) => res.toUpperCase())
        .replace(regLower, (match, res) => res.toLowerCase())
        .replace(regMix, (match, res) => toMixCase(res));
}

let text = 'We <upcase>are <mixcase>living</mixcase></upcase> in a <upcase>yellow submarine</upcase>. ' +
    'We <mixcase>don\'t</mixcase> have <lowcase>anything</lowcase> <upcase>else</upcase>.';
console.log(text);
console.log(changeText(text));
