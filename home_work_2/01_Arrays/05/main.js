let selectionSort = (array) => {
    let length = array.length;
    for (let i = 0; i < length - 1; i++) {
        let pos = i;
        for (let j = i + 1; j < length; j++) {
            if (array[j] < array[pos]) {
                pos = j;
            }
        }
        if (pos !== i) {
            let tmp = array[i];
            array[i] = array[pos];
            array[pos] = tmp;
        }
    }
};
let x = [7, 9, 2, 5, 3, 12, 1];
console.log(`Before sort [${x}]`);

selectionSort(x);
console.log(`After sort [${x}]`);