let compareCharArrays = function (firstArray, secondArray) {
    if (firstArray.length !== secondArray.length)
        return false;
    for (let i = 0; i < firstArray.length; i++) {
        if (firstArray[i] !== secondArray[i]) {
            return false;
        }
    }
    return true;
};
let arr1 = ['a', 'b', 'c'];
let arr2 = ['a', 'b', 'c'];
console.log(`arrays [${arr1}] and [${arr2}] are ${compareCharArrays(arr1, arr2) ? 'equal' : 'not equal'}`);