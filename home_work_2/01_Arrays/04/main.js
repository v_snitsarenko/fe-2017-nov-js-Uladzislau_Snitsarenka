let findGrowSequence = (array) => {
    let maxLength = 1;
    let maxIndex = 0;
    let currentLength = 1;
    array.reduce(function (prev, current, i) {
        currentLength = prev < current ? currentLength + 1 : 1;
        if (currentLength > maxLength) {
            maxLength = currentLength;
            maxIndex = i - currentLength + 1;
        }
        return current;
    });
    return array.slice(maxIndex, maxIndex + maxLength);
};
let array = [3, 2, 3, 4, 2, 2, 3, 4, 5, 6, 4];
console.log(`In [${array}] first grow sequence ${findGrowSequence(array)}`);