let binarySearch = (array, x) => {
    if ((array.length === 0) || (x < array[0]) || (x > array[array.length - 1]))
        return -1;
    let first = 0;
    let last = array.length;
    while (first < last) {
        let mid = Math.floor(first + (last - first) / 2);
        if (x <= array[mid])
            last = mid;
        else
            first = mid + 1;
    }
    if (array[last] === x) {
        return last;
    }
    else {
        return -1;
    }
};
let array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 15, 17, 40];
let elem = 17;
console.log(`element ${elem} in [${array}] found at position ${binarySearch(array, elem)}`);