let mostFrequentNumber = (array) => {
    let currentCount = 0;
    let maxValue;
    let maxCount = 0;
    for (let i = 0; i < array.length; i++) {
        currentCount = 0;
        for (let j = 1; j < array.length; j++) {
            if (array[i] === array[j]) {
                currentCount++;
            }
        }
        if (currentCount > maxCount) {
            maxCount = currentCount;
            maxValue = array[i];
        }
    }
    console.log(`most frequent value is ${maxValue} (${maxCount} times)`);
};
let array = [1, 2, 3, 4, 5, 6, 4, 3, 2, 2, 2, 2];
console.log(`in array [${array}]`);
mostFrequentNumber(array);