function init() {
    let canvas = document.getElementById("canvas");
    let ctx = canvas.getContext("2d");
    canvas.width = 1000;
    canvas.height = 600;
    drawBike(ctx);
}

function drawBike(ctx) {
    ctx.lineWidth = 4;
    ctx.strokeStyle = "#337D8F";
    ctx.fillStyle = "#90CAD7";
    ctx.arc(95, 250, 90, 0, 2*Math.PI);
    ctx.moveTo(280,240);
    ctx.arc(255, 240, 25, 0, 2*Math.PI);
    ctx.moveTo(535,245);
    ctx.arc(445, 245, 90, 0, 2*Math.PI);
    ctx.fill();
    ctx.moveTo(135,85);
    ctx.lineTo(215,85);
    ctx.moveTo(175,85);
    ctx.lineTo(255,240);
    ctx.lineTo(95,245);
    ctx.lineTo(200,125);
    ctx.lineTo(428,128);
    ctx.lineTo(415,60);
    ctx.lineTo(345,85);
    ctx.moveTo(465,3);
    ctx.lineTo(415,60);
    ctx.lineTo(450,240);
    ctx.moveTo(425,130);
    ctx.lineTo(250,240);
    ctx.moveTo(270,260);
    ctx.lineTo(285,280);
    ctx.moveTo(235,223);
    ctx.lineTo(220,200);
    ctx.stroke();
}

window.onload = init();