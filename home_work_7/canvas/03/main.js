function init() {
    let canvas = document.getElementById("canvas");
    let ctx = canvas.getContext("2d");
    canvas.width = 1000;
    canvas.height = 600;
    drawHouse(ctx);
}

function drawHouse(ctx) {
    const WINDOW = {width: 75, height: 50};
    ctx.strokeStyle = "black";
    ctx.moveTo(2, 250);
    ctx.lineTo(225, 0);
    ctx.lineTo(315, 100);
    ctx.lineTo(315, 190);
    ctx.lineTo(315, 65);
    ctx.bezierCurveTo(315, 50, 365, 50, 365, 65);
    ctx.bezierCurveTo(365, 80, 315, 80, 315, 65);
    ctx.bezierCurveTo(315, 50, 365, 50, 365, 65);
    ctx.lineTo(365, 190);
    ctx.lineTo(365, 150);
    ctx.lineTo(450, 250);
    ctx.lineTo(2, 250);
    ctx.lineTo(2, 590);
    ctx.lineTo(450, 590);
    ctx.lineTo(450, 250);
    ctx.moveTo(53, 590);
    ctx.lineTo(53, 470);
    ctx.moveTo(175, 590);
    ctx.lineTo(175, 470);
    ctx.moveTo(115, 590);
    ctx.lineTo(115, 432);
    ctx.lineWidth = 4;
    ctx.fillStyle = "#975B5B";
    ctx.fill();
    ctx.stroke();
    ctx.closePath();
    ctx.fillStyle = "black";
    ctx.fillRect(35, 295, WINDOW.width, WINDOW.height);
    ctx.fillRect(35, 350, WINDOW.width, WINDOW.height);
    ctx.fillRect(115, 350, WINDOW.width, WINDOW.height);
    ctx.fillRect(115, 295, WINDOW.width, WINDOW.height);
    ctx.fillRect(255, 295, WINDOW.width, WINDOW.height);
    ctx.fillRect(255, 350, WINDOW.width, WINDOW.height);
    ctx.fillRect(335, 350, WINDOW.width, WINDOW.height);
    ctx.fillRect(335, 295, WINDOW.width, WINDOW.height);
    ctx.fillRect(255, 435, WINDOW.width, WINDOW.height);
    ctx.fillRect(335, 435, WINDOW.width, WINDOW.height);
    ctx.fillRect(255, 495, WINDOW.width, WINDOW.height);
    ctx.fillRect(335, 495, WINDOW.width, WINDOW.height);
    ctx.beginPath();
    ctx.arc(135, 545, 6, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.beginPath();
    ctx.arc(95, 545, 6, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.moveTo(53, 475);
    ctx.bezierCurveTo(55, 420, 175, 420, 175, 472);
    ctx.stroke();

}

window.onload = init();