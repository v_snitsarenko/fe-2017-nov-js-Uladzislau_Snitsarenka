function init() {
    let canvas = document.getElementById("canvas");
    let ctx = canvas.getContext("2d");
    canvas.width = 1000;
    canvas.height = 600;
    drawHead(ctx);
}

function drawHead(ctx) {
    ctx.beginPath();
    ctx.lineWidth = 4;
    ctx.strokeStyle = "#22545F";
    ctx.fillStyle = "#90CAD7";
    ctx.ellipse(135, 250, 100, 110, 90 * Math.PI / 180, 0, 2 * Math.PI);
    ctx.fill();
    ctx.moveTo(110, 220);
    ctx.lineTo(85, 265);
    ctx.lineTo(110, 265);
    ctx.stroke();
    ctx.beginPath();
    ctx.ellipse(110, 305, 15, 40, 100 * Math.PI / 180, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.beginPath();
    ctx.fillStyle = "#22545F";
    ctx.ellipse(145, 215, 5, 10, 180 * Math.PI / 180, 0, 2 * Math.PI);
    ctx.ellipse(58, 215, 5, 10, 180 * Math.PI / 180, 0, 2 * Math.PI);
    ctx.fill();
    ctx.beginPath();
    ctx.ellipse(65, 217, 13, 19, 90 * Math.PI / 180, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.beginPath();
    ctx.ellipse(152, 217, 13, 19, 90 * Math.PI / 180, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.beginPath();
    ctx.strokeStyle = "#262626";
    ctx.fillStyle = "#396693";
    ctx.ellipse(130, 164, 23, 122, 90 * Math.PI / 180, 0, 2 * Math.PI);
    ctx.fill();
    ctx.stroke();
    ctx.beginPath();
    ctx.moveTo(70, 25);
    ctx.lineTo(70, 150);
    ctx.bezierCurveTo(70, 170, 200, 170, 200, 145);
    ctx.lineTo(200, 25);
    ctx.bezierCurveTo(200, 0, 70, 0, 70, 25);
    ctx.fill();
    ctx.bezierCurveTo(70, 40, 200, 40, 200, 25);
    ctx.stroke();
}

window.onload = init();