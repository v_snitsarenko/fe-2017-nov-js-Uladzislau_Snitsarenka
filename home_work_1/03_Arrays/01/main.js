const ARRAY_SIZE = 10;
let array = new Array(ARRAY_SIZE);

for(let i = 0; i < ARRAY_SIZE; i++) {
    array[i] = Math.floor( Math.random() < 0.5 ? Math.random() * -99 : Math.random() * 99 );
}

console.log(`Array = ${array}`);
console.log(`Max value = ${Math.max.apply(null, array)}`);
console.log(`Min value = ${Math.min.apply(null, array)}`);
console.log(`Total = ${array.reduce( (sum, elem) => sum + elem, 0)}`);
console.log(`Average = ${array.reduce( (sum, elem) => sum + elem, 0) / ARRAY_SIZE}`);
console.log(`Odd values = ${array.filter( (elem) => elem % 2 !== 0)}`);