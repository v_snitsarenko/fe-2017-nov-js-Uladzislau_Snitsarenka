const MATRIX_SIZE = 3;
let matrix = new Array(MATRIX_SIZE);

for (let i = 0; i < MATRIX_SIZE; i++) {
    matrix[i] = new Array(MATRIX_SIZE);
}
console.log('filling with random values');
for (let i = 0; i < MATRIX_SIZE; i++) {
    for (j = 0; j < MATRIX_SIZE; j++) {
        matrix[i][j] = Math.floor(Math.random() < 0.5 ? Math.random() * -9 : Math.random() * 9);
    }
}
console.log('replace values 10 or 20');
for (let i = 0; i < MATRIX_SIZE; i++) {
    for (let j = 0; j < MATRIX_SIZE; j++) {
        if (i === j) {
            matrix[i][j] = matrix[i][j] < 0 ? 10 : 20;
        }
    }
}
console.log(matrix);