let factorial = (number) => {
    let fact = 1;
    let i = 1;
    do {
        fact = fact * i;
        i++;
    } while (i <= number);
    return fact;
};
console.log(`5! = ${factorial(0)}`);
