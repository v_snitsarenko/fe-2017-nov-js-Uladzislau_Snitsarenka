let drawRhombus = (height) => {
    let i = 0;
    while (i < height/2) {
        let space = '';
        let star = '';
        for (let j = 0; j < height - i; j++) space += ' ';
        for (let j = 0; j < 2 * i + 1; j++) star += '*';
        console.log(space + star);
        i++;
    }
    while ( (i > height / 2) && (i < height)) {
        let space = '';
        let star = '';
        for (let j = 0; j < i + 1; j++) space += ' ';
        for (let j = height - i + (height-1 - i) ; j > 0; j--) star += '*';
        console.log(space + star);
        i++;
    }
};
drawRhombus(7);

let drawRectangle = (width, height) => {
    let result='';
    for (let i = 0; i < height; i++) {
        for (let j = 0; j < width; j++) {
            if (i === 0 || i === (height - 1)) {
                result += '*';
            } else {
                if (j === 0 || j === (width - 1)) {
                    result += '*';
                } else {
                    result += ' ';
                }
            }
        }
        result += '\n\r';
    }
    console.log(result);
};
drawRectangle(12,5);

let drawRightTriangle = (height)=> {
    let rightTriangle = '*\n';
    for (let i = 1; i < height; i++) {
        rightTriangle += '*';
        if((i > 0) && (i < height - 1)) {
            for(let j = 0; j < i; j++){
                rightTriangle += ' ';
            }
            rightTriangle += '*\n';
        } else {
            for(let z = 0; z <= i; z++){
                rightTriangle += '*';
            }
        }
    }
    console.log(rightTriangle);
};
drawRightTriangle(7);

let drawTriangle = (height) => {
    let i = 0;
    while (i < height) {
        let space = '';
        let star = '';
        for (let j = 0; j < height - i; j++) space += ' ';
        for (let j = 0; j < 2 * i + 1; j++) star += '*';
        console.log(space + star);
        i++;
    }
};
drawTriangle(7);