let a = 10, b = 20, c = 6;
let average = (...args) => args.reduce((sum, elem) => sum + elem, 0) / args.length;
console.log(`Average of ${a}, ${b}, ${c} = ${average(a, b, c)}`);