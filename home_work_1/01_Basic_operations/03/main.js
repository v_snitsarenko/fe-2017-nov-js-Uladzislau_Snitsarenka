let calculateCylinderV = (h, r) => Math.PI * Math.pow(r, 2) * h;
let calculateCylinderS = (h, r) => 2 * Math.PI * r * (r + h);
let cylinderH = 20;
let cylinderR = 4;
console.log(`Cylinder Square = ${calculateCylinderS(cylinderH,cylinderR)}`);
console.log(`Cylinder Volume = ${calculateCylinderV(cylinderH,cylinderR)}`);