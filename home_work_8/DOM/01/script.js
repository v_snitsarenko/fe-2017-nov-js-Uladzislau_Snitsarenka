function getInnerDivsQS() {
    return document.querySelectorAll('div div');
}

function getInnerDivsGE() {
    let divs = document.getElementsByTagName('div');
    let result = [];
    [].forEach.call(divs, div => {
        if (div.parentElement.nodeName === 'DIV') {
            result.push(div);
        }
    });
    return result;
}

function getInputValue() {
    console.log(document.querySelector('input[type="text"]').value);
}

function changeColor() {
    document.body.style.backgroundColor = document.querySelector('input[type="color"]').value;
}

console.dir(getInnerDivsQS());
console.dir(getInnerDivsGE());