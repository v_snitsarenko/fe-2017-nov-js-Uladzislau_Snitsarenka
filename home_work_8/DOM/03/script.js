function init() {
    const DIVS_COUNT = 9;
    const START_ANGLE = 90;
    const ANGLE_PERIOD = 360 / DIVS_COUNT;
    const MAIN_SIZE = 400;
    const MAIN_RADIUS = MAIN_SIZE / 2;
    const DIV_SIZE = 50;
    const DIV_RADIUS = DIV_SIZE / 2;
    let main = document.createElement('div');
    main.setAttribute('id', `main`);
    main.style.cssText =
        `height: ${MAIN_SIZE}px;
        width: ${MAIN_SIZE}px;
        position: absolute;
        border:1px solid black;
        border-radius: 50%;
        top: 10%;
        left: 30%;`;
    document.body.appendChild(main);

    let theta = [];
    for (let j = 0; j < 360; j += ANGLE_PERIOD) {
        theta.push(Math.radians(j));
    }
    let circleArray = [];
    console.dir(theta);
    for (let i = 0; i < DIVS_COUNT; i++) {
        circleArray[i] = document.createElement('div');
        let pos_x = Math.round(MAIN_RADIUS * (Math.cos(theta[i])));
        let pos_y = Math.round(MAIN_RADIUS * (Math.sin(theta[i])));
        circleArray[i].style.position = 'absolute';
        circleArray[i].style.backgroundColor = getRandomColor();
        circleArray[i].style.width = `${DIV_SIZE}px`;
        circleArray[i].style.height = `${DIV_SIZE}px`;
        circleArray[i].style.border = '2px solid black';
        circleArray[i].textContent = 'DIV';
        circleArray[i].style.transform = `rotate(${START_ANGLE - i * ANGLE_PERIOD}deg)`;
        circleArray[i].style.top = (MAIN_RADIUS - pos_y - DIV_RADIUS) + 'px';
        circleArray[i].style.left = (MAIN_RADIUS + pos_x - DIV_RADIUS) + 'px';
        main.appendChild(circleArray[i]);
    }
}

Math.radians = function (degrees) {
    return degrees * Math.PI / 180;
};

function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

let angle = 0;

function moveDivs() {
    document.getElementById(`main`).style.transform = `rotate(${angle}deg)`;
    if (angle > 360) {
        angle = 0;
    }
    angle += 5;
}

init();
setInterval(moveDivs, 100);


