function createDivs(number) {
    for (let i = 0; i < number; i++) {
        let div = document.createElement('div');
        let div_width = randomInteger(20, 100);
        let div_height = randomInteger(20, 100);
        div.style.width = `${div_width}px`;
        div.style.height = `${div_height}px`;
        div.style.backgroundColor = getRandomColor();
        div.style.display = 'flex';
        div.style.alignItems = 'center';
        div.style.color = getRandomColor();
        div.style.position = 'absolute';
        let posX = (Math.random() * (document.documentElement.clientWidth) - div_width).toFixed();
        let posY = (Math.random() * (document.documentElement.clientHeight) - div_height).toFixed();
        div.style.left = `${posX}px`;
        div.style.top = `${posY}px`;
        let strong = document.createElement('strong');
        strong.textContent = 'div';
        strong.style.margin = '0 auto';
        div.style.boxSizing = 'border-box';
        div.appendChild(strong);
        div.style.borderRadius = `${randomInteger(1, 50)}%`;
        div.style.borderColor = getRandomColor();
        div.style.borderStyle = 'solid';
        div.style.borderWidth = `${randomInteger(1, 20)}px`;
        document.body.appendChild(div);
    }
}

function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function randomInteger(min, max) {
    return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}

document.body.onload = createDivs(1000);
