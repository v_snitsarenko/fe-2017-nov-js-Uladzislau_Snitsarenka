let area = document.createElement('textarea');
area.setAttribute('cols', '30');
area.setAttribute('rows', '10');
let input1 = document.createElement('input');
input1.setAttribute('type', 'color');
let input2 = document.createElement('input');
input2.setAttribute('type', 'color');

document.body.appendChild(area);
document.body.appendChild(input1);
document.body.appendChild(input2);

input1.onchange = function () {
    area.style.color = this.value;
};
input2.onchange = function () {
    area.style.backgroundColor = this.value;
};
