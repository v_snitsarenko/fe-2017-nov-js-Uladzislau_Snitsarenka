let tree = [
    {
        name: 'item1',
        children: [{
            name: 'sub1',
            children: []
        },
            {
                name: 'sub2',
                children: []
            }
        ]
    },
    {
        name: 'item2',
        children: []
    },
    {
        name: 'item3',
        children: []
    },
    {
        name: 'item4',
        children: [
            {
                name: 'sub2 item 1',
                children: []
            }, {
                name: 'sub2 item 2',
                children: [
                    {
                        name: 'sub-sub 1 item 1',
                        children: [
                            {
                                name: 'sub-sub-sub 1 item 1',
                                children: []
                            }, {
                                name: 'sub-sub-sub 1 item 1',
                                children: []
                            }
                        ]
                    },
                    {
                        name: 'sub-sub 2 item 1',
                        children: []
                    }
                ]
            },
            {
                name: 'sub2 item 3',
                children: []
            }
        ]
    }
];

function createTreeView(tree) {
    let treeView = document.createElement('ul');
    tree.forEach((elem) => {
        let li = document.createElement('li');
        li.textContent = elem.name;
        const COUNT = elem.children.length;
        if (COUNT !== 0) {
            let sub_ul = createTreeView(elem.children);
            sub_ul.style.display = `none`;
            li.style.listStyle = 'square outside';
            li.setAttribute('class', 'dropDown');
            li.appendChild(sub_ul);
        }
        treeView.appendChild(li);
    });
    return treeView;
}

function showItems(event) {
    let child = event.target.childNodes[1];
    if (child.classList.contains('opened') === false) {
        child.style.display = 'block';
        child.classList.add('opened');
    } else {
        child.style.display = 'none';
        child.classList.remove('opened');
    }
}

let component = createTreeView(tree);
component.addEventListener('click', showItems);
document.body.appendChild(component);