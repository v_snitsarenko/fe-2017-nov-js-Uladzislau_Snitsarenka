let tags = [
    "cms", "javascript", "js", "ASP.NET MVC", ".net", ".net", "css", "wordpress", "xaml", "js", "http",
    "web", "asp.net", "asp.net MVC", "ASP.NET MVC", "wp", "javascript", "js", "cms", "html", "javascript",
    "http", "http", "CMS"
];

function generateTagCloud(tags, minFontSize, maxFontSize) {
    let result = document.createElement('div');
    result.style.width = '250px';
    result.style.border = '1px solid black';
    const SPACE = `&nbsp`.repeat(4);
    let tags_count = {};
    tags.forEach((item) => item in tags_count ? tags_count[item]++ : tags_count[item] = 1);
    let font_sizes = [...new Array(Math.max(...Object.values(tags_count)))].map(() => {
        return (randomInteger(minFontSize, maxFontSize));
    }).sort((a, b) => a - b);
    for (let item in tags_count) {
        let elem = document.createElement('span');
        elem.innerHTML = `${item}${SPACE}`;
        elem.style.display = 'inline-block';
        elem.style.fontSize = `${font_sizes[tags_count[item] - 1]}px`;
        result.appendChild(elem);
    }
    return result;
}

function randomInteger(min, max) {
    return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}

let tagCloud = generateTagCloud(tags, 12, 42);
document.body.appendChild(tagCloud);