if ('serviceWorker' in navigator) {
    navigator.serviceWorker
        .register('/sw.js')
        .then(function (registration) {
            console.log('ServiceWorker registration', registration);
        }).catch(function (err) {
        throw new Error('ServiceWorker error: ' + err);
    });
}