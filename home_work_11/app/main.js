function getRandomNumber() {
    return `${getRandomInt(1, 10)}${getRandomInt(0, 10)}${getRandomInt(0, 10)}${getRandomInt(0, 10)}`;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

let secretNumber = getRandomNumber();
let attemptsNumber = 0;

function getHint(secret, guess) {
    let bulls = 0;
    let cows = 0;
    let numbers = [...new Array(10)].fill(0);

    for (let i = 0; i < secret.length; i++) {
        let s = secret.charCodeAt(i) - 48;
        let g = guess.charCodeAt(i) - 48;
        if (s === g) bulls++;
        else {
            if (numbers[s] < 0) cows++;
            if (numbers[g] > 0) cows++;
            numbers[s]++;
            numbers[g]--;
        }
    }
    return {'A': bulls, 'B': cows};
}

function checkIsNumber(value) {
    return !!Number(value);
}

function checkResult() {
    let input = document.getElementById('guess');
    let value = input.value;
    if (checkIsNumber(value) && value.length === 4) {
        let res = getHint(secretNumber, value);
        if (res.A === 4) {
            document.getElementById('win').textContent += `You win`;
            input.setAttribute('disabled', 'true');
            input.setAttribute('placeholder', secretNumber);
            document.getElementById('check').setAttribute('disabled', 'true');
            alert('you win');
            let name = prompt('Enter your name');
            saveResultsToLS({'name': name, 'count': attemptsNumber});
        } else {
            document.getElementById('result').innerHTML += ` <tr>
            <td>${++attemptsNumber}</td>
            <td>${value}</td>
            <td>${res.A}A${res.B}B</td>
        </tr>`;
        }
        input.value = '';
    } else {
        alert('Enter correct four-digit number');

    }
}

function saveResultsToLS(data) {
    let res = loadResultsFromLS() || [];
    res.push(data);
    localStorage.setItem('results', JSON.stringify(res));
}

function loadResultsFromLS() {
    let results = JSON.parse(localStorage.getItem('results'));
    return results ? results : [];
}

function highScoreList() {
    let table = document.getElementById('high-score');
    if (table.style.display === 'none') {
        let results = loadResultsFromLS();
        results.sort((a, b) => {
            return a.count - b.count;
        });
        let html = `<tr>
            <th>Position</th>
            <th>Name</th>
            <th>Result</th>
        </tr>`;
        let position = 1;
        results.forEach(elem => {
            html += `<tr>
            <td>${position++}</td>
            <td>${elem.name}</td>
            <td>${elem.count}</td>
        </tr>`;
        });
        table.style.display = 'table';
        table.innerHTML = html;
    } else {
        table.style.display = 'none';
    }
}

function enterResult(e) {
    if (e.keyCode === 13) {
        checkResult();
    }
}

function startNewGame() {
    attemptsNumber = 0;
    secretNumber = getRandomNumber();
    document.getElementById('win').textContent = ``;
    document.getElementById('guess').removeAttribute('disabled');
    document.getElementById('guess').removeAttribute('placeholder');
    document.getElementById('check').removeAttribute('disabled');
    document.getElementById('result').innerHTML = `<tr>
            <th>№</th>
            <th>Guess</th>
            <th>Result</th>
        </tr>`;
}

document.getElementById('check').addEventListener('click', checkResult);
document.getElementById('guess').addEventListener('keypress', enterResult);
document.getElementById('list').addEventListener('click', highScoreList);
document.getElementById('newGame').addEventListener('click', startNewGame);