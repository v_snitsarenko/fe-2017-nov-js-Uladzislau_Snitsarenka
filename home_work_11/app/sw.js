let CACHE_NAME = '4digits-game-v4';
let cacheUrls = [
    '/index.html',
    '/styles.css',
    '/main.js'
];
let MAX_AGE = 0;
self.addEventListener('install', function (event) {
    event.waitUntil(
        caches.open(CACHE_NAME).then(function (cache) {
            return cache.addAll(cacheUrls);
        })
    );
});

self.addEventListener('activate', function (event) {
    console.log('activate', event);
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.match(event.request).then(function(cachedResponse) {
            let lastModified, fetchRequest;
            if (cachedResponse) {
                lastModified = new Date(cachedResponse.headers.get('last-modified'));
                if (lastModified && (Date.now() - lastModified.getTime()) > MAX_AGE) {
                    fetchRequest = event.request.clone();
                    return fetch(fetchRequest).then(function(response) {
                        let resp_temp = response.clone();
                        if (!response || response.status !== 200) {
                            return cachedResponse;
                        }
                        caches.open(CACHE_NAME).then(function(cache) {
                            cache.put(event.request, resp_temp);
                        });

                        return response;
                    }).catch(function() {
                        return cachedResponse;
                    });
                }
                return cachedResponse;
            }
            return fetch(event.request);
        })
    );
});