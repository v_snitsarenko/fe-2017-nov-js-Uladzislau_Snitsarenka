const port = 3000;
const express = require('express');
const app = express();

let apiRouter = express.Router();

app.use(express.static(__dirname + '/app'));

app.use('/api', apiRouter);

app.get('*', function (req, res) {
    res.send(req.url);
});

app.listen(port);

console.log('Server started at ' + port);