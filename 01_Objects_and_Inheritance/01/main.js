let instanceOf = function (inst, clazz) {
    let result = false;
    while (!result) {
        if (!inst) {
            break;
        } else {
            inst = inst.__proto__;
            if (clazz.prototype === inst) {
                result = true;
            }
        }
    }
    return result;
};

class A {
}

class B extends A {
}

class C extends A {
}

class D extends B {
}

let arr = [];
console.log(instanceOf(new B(), A));
console.log(instanceOf(new C(), A));
console.log(instanceOf(new B(), B));
console.log(instanceOf(new B(), C));
console.log(instanceOf(new C(), B));
console.log(instanceOf(arr, Object));

console.log(new D() instanceof A);
console.log(instanceOf(new D(), A));