function createFunctions(size = 5) {
    let arr = [];
    for (let i = 0; i < size; i++) {
        arr.push(function callback() {
            return i;
        });
    }
    return arr;
}

let callbacks = createFunctions();
console.log('let callbacks = createFunctions();');
console.log(`callbacks[1](); -> ${callbacks[1]()}`);
console.log(`callbacks[3](); -> ${callbacks[3]()}`);