function add(a) {
    function plus(b) {
        return a + b;
    }

    return plus;
}

let a = add(2)(3);
let b = add(5)(1);
console.log(`let a = add(2)(3),  a = ${a}`);
console.log(`let b = add(5)(1),  b = ${b}`);