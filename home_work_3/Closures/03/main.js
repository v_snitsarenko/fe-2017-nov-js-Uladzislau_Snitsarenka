function createCounter(initValue = 0) {
    function count() {
        return initValue++
    }

    return count;
}

let counter1 = createCounter();
console.log('let counter1 = createCounter();');
console.log('counter1(); ->',counter1());
console.log('counter1(); ->',counter1());
console.log('counter1(); ->',counter1());

let counter2 = createCounter(13);
console.log('let counter2 = createCounter(13);');
console.log('counter2(); ->',counter2());
console.log('counter2(); ->',counter2());
console.log('counter2(); ->',counter2());