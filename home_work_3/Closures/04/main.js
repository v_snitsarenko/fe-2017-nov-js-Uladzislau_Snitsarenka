function createStorage() {
    let storage = {};

    function setItem(item, value) {
        storage[item] = value;
    }

    function getItem(item) {
        return item in storage ? storage[item] : null;
    }

    function hasItem(item) {
        return item in storage;
    }

    function length() {
        return Object.keys(storage).length;
    }

    function clear() {
        storage = {};
    }

    function removeItem(item) {
        delete storage[item];
    }

    return {
        setItem: setItem,
        getItem: getItem,
        hasItem: hasItem,
        length: length,
        clear: clear,
        removeItem: removeItem
    };
}

let storage = createStorage();
console.log('storage.setItem(\'userName\', \'Alex\');');
storage.setItem('userName', 'Alex');
console.log('storage.getItem(\'userName\') ->', storage.getItem('userName')); // 'Alex'
console.log('storage.getItem(\'userAge\') ->', storage.getItem('userAge')); // null
console.log('storage.hasItem(\'userAge\') ->', storage.hasItem('userAge')); // false
storage.setItem('userAge', 28);
console.log('storage.setItem(\'userAge\', 28);');
console.log('storage.hasItem(\'userAge\') ->', storage.hasItem('userAge')); // true
console.log('storage.length(); ->', storage.length());
storage.removeItem('userName');
console.log('storage.removeItem(\'userName\');');
console.log('storage.length() ->', storage.length());
storage.clear();
console.log('storage.clear();');
console.log('storage.length(); ->', storage.length());