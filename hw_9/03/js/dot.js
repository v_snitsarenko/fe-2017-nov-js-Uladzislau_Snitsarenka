class Dot {
    constructor(positions, speed) {
        this.positions = Object.assign({}, positions);

        this.speed = {
            dX: speed.dX,
            dY: speed.dY
        };

        this.radius = config.dot.radius;
    }

    move() {
        this.positions.x += this.speed.dX;
        this.positions.y += this.speed.dY;
    }


}