const config = {
    mouse:
        {
            current_x: 0,
            current_y: 0,
            dX: 0,
            dY: 0,
            oldX: 0,
            oldY: 0,
            update: function () {
                this.dX = (this.current_x - this.oldX) / 10;
                this.dY = (this.current_y - this.oldY) / 10;
                this.oldX = this.current_x;
                this.oldY = this.current_y;
            }
        },
    dot: {
        color: '#c2c2c2',
        minCount: 100,
        maxSpeed: 4,
        radius: 2,
        dotsByClick: 11,
        speedOffset : 2
    },
    line: {
        opacity: {
            strong: 1,
            weak: 0.2
        },
        maxLength: 100
    },
    updateInterval: 50,
    removeInterval: 500
};