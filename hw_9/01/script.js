window.onload = function () {
    let item = document.querySelectorAll('#carousel img')[0];
    init();

    function init() {
        let current = document.querySelector('#line img.current');
        item.src = current.src;
    }

    function changeCurrent(e) {
        document.querySelector('#line img.current').classList.remove('current');
        item.src = e.target.src;
        e.target.classList += " current";

    }

    let left = 0;


    function next() {
        let elem = document.querySelectorAll('.item');
        let current = document.querySelector('.current');
        setTimeout(function () {
            let line = document.getElementById('line');
            left = left - 128;
            if (left < -512) {
                left = 0;

            }
            line.style.left = left + "px";
        }, 100);


        let next = current.nextElementSibling;
        if (next) {
            next.classList += " current";

            next.classList += " current";
        }
        else {


            elem[0].classList += " current";
        }
        current.classList.remove('current');
        init();
    }

    function prev() {
        setTimeout(function () {
            let line = document.getElementById('line');
            left = left + 128;
            if (left > 0) {
                left = -512;

            }
            line.style.left = left + "px";
        }, 100);
        let current = document.querySelector('.current');

        let prev = current.previousElementSibling;
        if (prev) {
            prev.classList += " current";
        }
        else {
            let elem = document.querySelectorAll('.item');
            elem[elem.length - 1].classList += " current";
        }
        current.classList.remove('current');
        init();
    }

    document.querySelector('.prev').addEventListener('click', prev);
    document.querySelector('.next').addEventListener('click', next);
    let items = document.querySelectorAll('#line img');

    items.forEach(function (element) {
        element.addEventListener('click', changeCurrent);
    });

};