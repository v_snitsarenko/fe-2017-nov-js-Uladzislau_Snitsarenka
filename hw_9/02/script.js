function newElement() {
    let li = document.createElement("li");
    let inputValue = document.getElementById("newItem").value;
    let label = document.createElement('label');
    let input = document.createElement('input');
    input.setAttribute('type', 'checkbox');
    let span = document.createElement('span');
    let t = document.createTextNode(inputValue);
    span.appendChild(t);
    label.appendChild(input);
    label.appendChild(span);
    li.appendChild(label);
    if (inputValue === '') {
        alert("You must write something!");
    } else {
        document.querySelector("ul").appendChild(li);
    }
    document.getElementById("newItem").value = "";

}

function enterElement(e) {
    if (e.keyCode === 13) {
        newElement();
    }
}

function getAllCheckedElements() {
    return document.querySelectorAll("li>label>input:checked");
}

function deleteElements() {
    getAllCheckedElements().forEach(elem => {
        elem.parentElement.parentNode.remove();
    });
}

function hideElements() {
    getAllCheckedElements().forEach(elem => {
        elem.parentElement.parentNode.style.display = 'none';
    });
}

function showElements() {
    getAllCheckedElements().forEach(elem => {
        elem.parentElement.parentNode.style.display = 'block';
    });
}

document.getElementById('del').addEventListener('click', deleteElements);
document.getElementById('show').addEventListener('click', showElements);
document.getElementById('hide').addEventListener('click', hideElements);
document.getElementById('add').addEventListener('click', newElement);
document.getElementById("newItem").addEventListener('keypress', enterElement);