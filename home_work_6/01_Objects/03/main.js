let domElement = {};
Object.defineProperty(domElement, 'init', {
    value: function (type) {
        if (typeof type !== 'string') {
            throw new Error('type should be string only');
        }
        this.type = type;
        this.attributes = [];
        this.children = [];
        this.prototype = domElement;
        let content = '';
        Object.defineProperty(this, 'content', {
            set: function (newValue) {
                if (this.children.length === 0) {
                    content = newValue;
                } else content = '';
            },
            get: function () {
                return content;
            },
            enumerable: true
        });
        let parent = {};
        Object.defineProperty(this, 'parent', {
            set: function (elem) {
                if (elem.prototype === domElement) {
                    parent = elem;
                } else throw new Error('domElement expected');
            },
            get: function () {
                return parent;
            },
            enumerable: true
        });
        Object.defineProperty(this, 'innerHTML', {
            get: function () {
                let inner = `<${this.type}`;

                this.attributes.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
                this.attributes.forEach((elem) => {
                    inner += ` ${elem.name}="${elem.value}"`;
                });
                inner += '>';
                this.children.forEach((elem) => {
                    inner += `${elem.innerHTML}`;
                });
                return `${inner}${this.content}</${this.type}>`;
            }

        });
        Object.defineProperty(this, 'addAttribute', {
            value: function (name, value) {
                if (typeof name === 'string' && typeof value === 'string') {
                    this.attributes.push({name: name, value: value});
                } else throw new Error('attribute should be string only');
                return this;
            }

        });
        Object.defineProperty(this, 'appendChild', {
            value: function (elem) {
                if (typeof elem === 'string') {
                    this.children.push(Object.create(domElement).init(elem));
                    this.content = '';
                } else if (elem.prototype === domElement) {
                    this.children.push(elem);
                    this.content = '';
                } else throw new Error('domElement or string expected');
                return this;
            }

        });
        Object.defineProperty(this, 'removeAttribute', {
            value: function (attribute) {
                this.attributes.forEach((elem, i) => {
                    if (attribute === elem.name) {
                        this.attributes.splice(i, 1);
                    } else throw new Error('attribute does not exist')
                });
            }

        });
        return this;
    }
});


let meta = Object.create(domElement)
    .init('meta')
    .addAttribute('charset', 'utf-8');

let head = Object.create(domElement)
    .init('head')
    .appendChild(meta);

let div = Object.create(domElement)
    .init('div')
    .addAttribute('style', 'font-size: 42px');

div.content = 'Hello, world!';

let body = Object.create(domElement)
    .init('body')
    .appendChild(div)
    .addAttribute('id', 'myId')
    .addAttribute('color', '#012345');

let root = Object.create(domElement)
    .init('html')
    .appendChild(head)
    .appendChild(body);

//try this
//meta.removeAttribute('charset');
//meta.addAttribute(3); //error
//div.appendChild(Object.create(domElement).init('p'));
//div.content = 'Have a nice day';
//div.appendChild({}); //error

console.log(root.innerHTML);