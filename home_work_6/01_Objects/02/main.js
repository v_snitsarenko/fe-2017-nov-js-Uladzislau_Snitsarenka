function Person() {
}

function checkValue(val) {
    if (val.length < 3 || val.length > 20)
        throw new Error('Value must be between 3 and 20 characters');
    if (typeof val !== 'string')
        throw new Error('Value can be string only');
    return true;
}

Person.prototype.setFirstName = function (firstName) {
    if (checkValue(firstName))
        this.firstName = firstName;
};
Person.prototype.setLastName = function (lastName) {
    if (checkValue(lastName))
        this.lastName = lastName;
};
Person.prototype.fullName = function (fullName) {
    if (!arguments.length) return this.firstName + ' ' + this.lastName;
    var names = fullName.split(' ');
    var firstName = names[0];
    var lastName = names[1];
    this.setFirstName(firstName);
    this.setLastName(lastName);
};
Person.prototype.setAge = function (age) {
    if ((typeof age !== 'number' && !Number(age))) throw new Error('Incorrect age format');
    if (Number(age) < 0 || Number(age) > 150) throw new Error('Age value must be beetween 0 and 150');
    this.age = +age;
};
Person.prototype.introduce = function () {
    return 'Hello! My name is ' + this.fullName() + ' and I am ' + this.age + '-years-old';
};

var person = new Person();
person.setFirstName('Ivan');
person.setLastName('Sidorov');
console.log(person.fullName());
person.fullName('Ivan Petrov');
person.setAge('12');
console.log(person.fullName());
console.log(person.introduce());