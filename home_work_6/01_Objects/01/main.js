class Person {
    constructor(firstName, lastName, age) {

        if ((firstName.length < 3) || (firstName.length > 20) || (lastName.length < 3) || (lastName.length > 20))
            throw new Error('Firstname or Lastname must be between 3 and 20 characters');

        if ((typeof firstName !== 'string') || (typeof lastName !== 'string'))
            throw new Error('Firstname or Lastname can be string only');

        if ((typeof age !== 'number' && !Number(age))) throw new Error('Incorrect age format');

        if (Number(age) < 0 || Number(age) > 150) throw new Error('Age value must be beetween 0 and 150');

        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;

    }

    fullName(fullName) {
        if (!arguments.length) return this.firstName + ' ' + this.lastName;
        let names = fullName.split(' ');
        this.firstName = names[0];
        this.lastName = names[1];
    }

    introduce() {
        return 'Hello! My name is ' + this.fullName() + ' and I am ' + this.age + '-years-old';
    }
}

const petya = new Person('Ivan', 'Ivanov', '12');
console.log(petya);
console.log(petya.fullName());
petya.fullName('Petr Sidorov');
console.log(petya.introduce());