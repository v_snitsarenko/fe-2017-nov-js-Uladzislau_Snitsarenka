let comp = function (a, b) {
    return a === b ? 1 : -1;
};
console.log(`comp('abc', 'abc') ->  ${comp('abc', 'abc')}`);
console.log(`comp('abC', 'abc') ->  ${comp('abC', 'abc')}`);