function find(testString, test = testString) {
    return testString.indexOf(test);
}

console.log(`find('abc','b') -> ${find('abc', 'b')}`);
console.log(`find('abc') -> ${find('abc')}`);
console.log(`find('abc', 'd') -> ${find('abc', 'd')}`);
console.log(`find('abc', 'a', 'd') -> ${find('abc', 'a', 'd')}`);