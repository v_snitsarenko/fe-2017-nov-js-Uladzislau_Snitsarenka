function toConsole(param) {
    console.log(param);
}

function toAlert(param) {
    alert(param);
}

function splitWords(msg, callback) {
    let res = msg.split(/\W+/);
    if (!callback) {
        return res;
    }
    else res.forEach(elem => callback(elem));
}

splitWords('My very. long, text! msg', toConsole);
splitWords('My very long text msg', toAlert);
console.log(splitWords('My very long text msg'));