function copyright() {
    const COPYRIGHT = '\u00A9';
    return function (param) {
        return COPYRIGHT + " " + param;
    };
}

console.log(`copyright()('EPAM') -> ${copyright()('EPAM')}`);