let fibo = function fib(n) {
    return Math.floor((Math.pow(((1 + Math.sqrt(5)) / 2), n) / Math.sqrt(5) + 0.5));
};
console.log(`fibo(2) -> ${fibo(2)}`);
console.log(`fibo(3) -> ${fibo(3)}`);
console.log(`fibo(7) -> ${fibo(7)}`);