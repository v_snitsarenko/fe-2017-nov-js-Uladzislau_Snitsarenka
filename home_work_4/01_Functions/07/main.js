let param1 = 'This is the first sentence. This is a sentence with a list of items: cherries, oranges, apples, bananas.';
let param2 = 'This is the second sentence. This is a sentence with a list of items: red, blue, yellow, black.';

let parts2 = (...args) => args.map((elem, i) => (elem.match(/(?::\s)(.*)(?:\.)/))[1]);
console.log(parts2(param1, param2));