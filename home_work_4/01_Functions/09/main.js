function str(string) {
    (!string) ? alert('String is empty') : alert('String is non empty');
}

str.isNonEmptyStr = function (str) {
    return !(!str || typeof str !== 'string');
};

str();
str('a');
console.log(`str.isNonEmptyStr() -> ${str.isNonEmptyStr()}`);
console.log(`str.isNonEmptyStr('') -> ${str.isNonEmptyStr('')}`);
console.log(`str.isNonEmptyStr('a') -> ${str.isNonEmptyStr('a')}`);
console.log(`str.isNonEmptyStr(1) -> ${str.isNonEmptyStr(1)}`);
