const http = require('http');
const port = 3000;
const fs = require('fs');

let json_data = [];
for(let i=1;i<5;i++) {
    fs.readFile(`data/data${i}.json`, 'utf8', function (err, data) {
        if (err) throw err;
        json_data[i]=(data);
    });
}

const requestHandler = (request, response) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    if (request.method === 'OPTIONS') {
        response.writeHead(200);
        response.end();
        return;
    }
    switch (request.url) {
        case '/data1.json':
            response.end(json_data[1]);
            break;
        case '/data2.json':
            response.end(json_data[2]);
            break;
        case '/data3.json':
            response.end(json_data[3]);
            break;
        case '/data4.json':
            response.end(json_data[4]);
            break;
        default:
            response.end("Incorrect URL. Not found 404");
            break;
    }
};
const server = http.createServer(requestHandler);
server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
});