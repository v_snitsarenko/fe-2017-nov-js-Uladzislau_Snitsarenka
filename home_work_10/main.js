function list(list) {
    let html = `<div class="container">
    <div class="header"><span>${list.title}</span></div>
    <input class="newItem" type="text" placeholder="Write what you want to do">
    <button class="add" type="button">Add</button>
    <button class="del" type="button" >Delete Selected</button>
    <button class="show" >Show selected</button>
    <button class="hide">Hide selected</button>
    <ul>`;
    for (let i = 0; i < list.todoList.length; i++) {
        html += `<li>
            <label>
                <input type="checkbox" ${list.todoList[i].done ? "checked" : ''}>
                <span>${list.todoList[i].todo}</span>
            </label>
        </li>`;
    }
    html += `</ul>
</div>`;

    document.body.innerHTML += html;
}

let delay = (time) => (result) => new Promise(resolve => setTimeout(() => resolve(result), time));
for (let i = 1; i < 5; i++) {
    fetch(`http://localhost:3000/data${i}.json`)
        .then(function (response) {
            return response.json();
        })
        .then(delay(i * 1000))
        .then(function (data) {
            list(data);
        })
        .then(addListeners)
        .catch(console.log);
}

function newElement(element) {
    let li = document.createElement("li");
    let inputValue = element.value;
    let label = document.createElement('label');
    let input = document.createElement('input');
    input.setAttribute('type', 'checkbox');
    let span = document.createElement('span');
    let t = document.createTextNode(inputValue);
    span.appendChild(t);
    label.appendChild(input);
    label.appendChild(span);
    li.appendChild(label);
    if (inputValue === '') {
        alert("You must write something!");
    } else {
        element.closest('.container').querySelector("ul").appendChild(li);
    }
    element.value = "";

}

function clickElem(e) {
    newElement(e.path[0].closest('.container').querySelector('.newItem'));
}

function enterElement(e) {
    if (e.keyCode === 13) {
        newElement(e.target);
    }
}

function deleteElements(evt) {
    let container = evt.target.closest(".container");
    container.querySelectorAll("li>label>input:checked").forEach(elem => {
        elem.parentElement.parentNode.remove();
    });
}

function hideElements(evt) {
    let container = evt.target.closest(".container");
    container.querySelectorAll("li>label>input:checked").forEach(elem => {
        elem.parentElement.parentNode.style.display = 'none';
    });
}

function showElements(evt) {
    let container = evt.target.closest(".container");
    container.querySelectorAll("li>label>input:checked").forEach(elem => {
        elem.parentElement.parentNode.style.display = 'block';
    });
}

function addListeners() {
    document.querySelectorAll('.del').forEach(elem => elem.addEventListener('click', deleteElements));
    document.querySelectorAll('.show').forEach(elem => elem.addEventListener('click', showElements));
    document.querySelectorAll('.hide').forEach(elem => elem.addEventListener('click', hideElements));
    document.querySelectorAll('.add').forEach(elem => elem.addEventListener('click', clickElem));
    document.querySelectorAll(".newItem").forEach(elem => elem.addEventListener('keypress', enterElement));
}



